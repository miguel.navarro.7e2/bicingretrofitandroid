package com.example.retrofitbicing.service;


import com.example.retrofitbicing.bicing.Stations;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface BicingService {
    @GET("station/id/{id}.json")
    Call<Stations> getStationById(@Path("id") String id);
}
