package com.example.retrofitbicing.bicing;

import com.google.gson.annotations.SerializedName;

public class Stations {
    @SerializedName("data")
    Data data;
    @SerializedName("code")
    int code;

    public Stations(Data data, int code) {
        this.data = data;
        this.code = code;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
