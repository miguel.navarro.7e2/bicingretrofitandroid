package com.example.retrofitbicing.bicing;

import com.google.gson.annotations.SerializedName;

public class Data {
    @SerializedName("bici")
    Bici bici;

    public Data(Bici bici) {
        this.bici = bici;
    }

    public Bici getBici() {
        return bici;
    }

    public void setBici(Bici bici) {
        this.bici = bici;
    }
}
