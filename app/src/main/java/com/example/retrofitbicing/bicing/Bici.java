package com.example.retrofitbicing.bicing;

import com.google.gson.annotations.SerializedName;

public class Bici {
    @SerializedName("id")
    private String id;
    @SerializedName("name")
    private String name;
    @SerializedName("lat")
    private String lat;
    @SerializedName("lon")
    private String lon;
    @SerializedName("nearby_stations")
    private String nearby_stations;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNearby_stations() {
        return nearby_stations;
    }

    public void setNearby_stations(String nearby_stations) {
        this.nearby_stations = nearby_stations;
    }
}
