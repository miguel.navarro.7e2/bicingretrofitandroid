package com.example.retrofitbicing.model;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.retrofitbicing.R;
import com.example.retrofitbicing.bicing.Bici;
import com.example.retrofitbicing.bicing.Data;
import com.example.retrofitbicing.bicing.Stations;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NearStationsFragment extends Fragment {

    private MainViewModel mViewModel;
    private String number;
    @BindView(R.id.nearStations)
    TextView nearStations;
    LiveData<Stations> mainStation;
    LiveData<Stations> nearbyStations;
    String allNames;
    LiveData<String> test;

    public static NearStationsFragment newInstance() {
        return new NearStationsFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.near_stations_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(MainViewModel.class);
        mainStation = mViewModel.getStationById(number);
        mainStation.observe(this, this::onNumberEntered);


    }

    private void onNumberEntered(Stations stations) {
        Data data = stations.getData();
        Bici bici = data.getBici();
        String nearStationsNum = bici.getNearby_stations();
        String[] nameOfNearStations = nearStationsNum.split(",");
        for (int i = 0; i<nameOfNearStations.length; i++) {
            String num = "";
            num = nameOfNearStations[i];
            nearbyStations = mViewModel.getStationById(num);
            nearbyStations.observe(this, this::onName);
        }
    }


    private void onName(Stations stations) {
        Data data = stations.getData();
        Bici bici = data.getBici();
        String nearStationsNum = bici.getName();
        allNames+=nearStationsNum + " ";
        nearStations.setText(allNames);
    }



    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        number = NearStationsFragmentArgs.fromBundle(getArguments()).getNumberStation();
    }
}
