package com.example.retrofitbicing.model;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.retrofitbicing.bicing.Stations;
import com.example.retrofitbicing.repository.BicingRepository;

public class MainViewModel extends AndroidViewModel {
    BicingRepository repository;

    public MainViewModel(@NonNull Application application) {
        super(application);
        repository = new BicingRepository(application);
    }
    public MutableLiveData<Stations> getStationById(String id) {
        return repository.getStationsById(id);
    }
}
