package com.example.retrofitbicing.repository;

import android.app.Application;
import android.widget.Toast;

import androidx.lifecycle.MutableLiveData;

import com.example.retrofitbicing.bicing.Stations;
import com.example.retrofitbicing.service.BicingService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class BicingRepository {
    BicingService service;
    Application application;

    public BicingRepository(Application application) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://barcelonaapi.marcpous.com/bicing/").addConverterFactory(GsonConverterFactory.create())
                .build();
        this.service = retrofit.create(BicingService.class);
        this.application = application;
    }

    public MutableLiveData<Stations> getStationsById(String id) {
        MutableLiveData<Stations> station = new MutableLiveData<>();
        Call<Stations> call = service.getStationById(id);
        call.enqueue(new Callback<Stations>() {

            @Override
            public void onResponse(Call<Stations> call, Response<Stations> response) {
                station.postValue(response.body());
            }

            @Override
            public void onFailure(Call<Stations> call, Throwable t) {
                Toast.makeText(application.getApplicationContext(),t.getMessage(),Toast.LENGTH_LONG).show();
            }
        });
        return station;
    }
}